function showNav(){
    document.getElementById("navBar").style = "display: block;";
    document.getElementById("btn-showNav").style = "display: none;";
    document.getElementById("btn-closeNav").style = "display: block;";
}
function closeNav(){
    document.getElementById("navBar").style = "display: none;";
    document.getElementById("btn-showNav").style = "display: block;";
    document.getElementById("btn-closeNav").style = "display: none;";
}

window.addEventListener('scroll', function(){
    const mainNav = document.getElementById("bgTile");
    const logoTitle = document.querySelector(".bgTile h1");
    const goUp = document.getElementById("btn-goUp");

    if(window.pageYOffset > 0){
        if(window.innerHeight < window.innerWidth){
         mainNav.style = "height:15vh;";
        }
        else{
            mainNav.style = "height:10vh;"; 
        }
            
        goUp.style = "opacity: .8";
        logoTitle.style = "font-size: 25px;";
    }
    else{
        mainNav.style = "height:20vh;";
        logoTitle.style = "font-size: 40px;";
        goUp.style = "opacity: 0";
    }
})


function myFunction(imgs) {
  var expandImg = document.getElementById("expandedImg");
  expandImg.src = imgs.src;
  expandImg.parentElement.style = "display:block;";
}

function showGallery(){
    document.getElementById("cake2").style = "display:block;";
    document.getElementById("btn-hide").style = "display:block;";
    document.getElementById("btn-seeAll").style = "display:none;";
}
function hideGallery(){
    document.getElementById("cake2").style = "display:none;";
    document.getElementById("btn-hide").style = "display:none;";
    document.getElementById("btn-seeAll").style = "display:block;";
}